# Sprints en cours (Mars 2018)

## Editeur

* [Ajouter l'interaction Long Tap](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/318)
* [Ajout de deux figurés dans les DERi pour les menu filtres et le menu principal](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/255)
* [Correction menu contextuel](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/323)
* [Agrandir le nombre d'action annulable](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/325)
* [Pouvoir dessiner un triangle équilatéral](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/269)
* [Aimanter sur les axes à 90°](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/268)
* [Correction anomalie suppression d'interaction avec Entrée](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/319)
* [Correction non suppression des interactions lors d'un nouveau DER](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/322)
* [Import / Export des fichiers MP3](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/230)
* [Lire les fichiers MP3 dans l'éditeur](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/326)
* [Correction anomalie data OSM quand ajout POI](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/324)

## Lecteur

* [Ajout d'un délai de déclenchement de l'interaction](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/102)
* [Ajout de la gestion du trigger 'long tap'](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/100)
* [Gestion de la langue FR sur Android](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/105)
* [Changer la phrase énoncée lors de la sélection d'un filtre](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/101)
* [Ajouter paramètres dans le menu](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/11)
* [Ajouter gestion nouveaux menu figurés](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/103)


# [Février 2018](./2018-02)

# [Janvier 2018](./2018-01)

# [Décembre 2017](./2017-12)