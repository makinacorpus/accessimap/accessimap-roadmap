# Import d'un DER au sein de l'éditeur

Ce document décrit les fonctionnalités d'import
que propose l'éditeur DER et les limitations à connaître.

## Import d'un DER au format ZIP

Il est possible de ré-importer un DER
qui aurait été exporté par ce même outil sous la forme d'une archive zip.
Cette opération efface tout le dessin existant
et le remplace par les données de l'archive fournie,
en plaçant le nouveau dessin à sa position et à sa taille d'origine.

**Important :**
Les fichiers générés au sein de l'archive
(et plus particulièrement les fichiers SVG)
n'ont pas vocation à être modifiés par des logiciels tiers
avant ré-importation dans l'éditeur DER.

Ces derniers sont traités et chargés différemment des fichiers SVG externes
et les modifier risque de donner des résultats inattendus
lors du ré-import dans l'éditeur DER.

Si modifier un tel fichier était nécessaire,
il faudrait s'assurer de ne pas en altérer la structure
et à insérer les nouveaux éléments dans les groupes existants
(notamment `drawing-layer` & `lines-layer`)
en sachant que toute propriété ajoutée à ces groupes
(propriété `transform` par exemple) sera ignorée lors de l'import.

## Import d'un fichier SVG externe

Il est également possible d'importer un fichier SVG seul.
Cette opération efface tout le dessin existant
et le remplace par les données de l'archive fournie.

**Important :** Lors de l'import d'un fichier SVG externe :

* Il est positionné aux coordonnées `(0,0)`,
  soit calé en haut à gauche de la page du dessin.
* Ses propriétés de taille sont utilisées
  pour dimensionner le dessin sur la page.
  Si le SVG importé est au format A4,
  il sera dimensionné de façon à correspondre
  à la taille d'une page A4 dans l'éditeur DER.
  Si ses dimensions sont exprimées en pixels,
  alors c'est la taille standard du pixel de référence selon le W3C
  qui sera pris en compte pour calculer la taille du dessin
  (voir [W3C - The reference pixel](https://www.w3.org/TR/css3-values/#reference-pixel)
  pour plus de détails sur le pixel de référence).
