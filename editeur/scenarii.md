# Scénarii de tests

## 1 Dessin simple

* ouvrir l'éditeur
* dessiner plusieurs formes géométriques à partir du menu dessin

## 2 Import de données OSM

* ouvrir l'éditeur
* importer les parcs sur la zone carto affichée, à partir du menu carto

## 3 Export du dessin

* reprendre les scenarii précédents
* exporter le dessin

## 4 Import du dessin

* importer le dessin précédent

## 5 Import du dessin après modification Inkscape

* ouvrir le dessin exporté dans Inkscape
* ajouter une forme conformément aux explications fournies [ici](./import.md)
* mettre à jour le zip du DERi
* importer le nouveau zip dans l'éditeur

## 6 Interactions

* ouvrir l'éditeur
* dessiner plusieurs formes géométriques à partir du menu dessin
* ajouter une interaction de type tap
* ajouter une interaction de type double tap sur une autre forme
* exporter le dessin
* tester le dessin dans le lecteur

## 7 Interactions avec données OSM

* ouvrir l'éditeur
* importer des formes OSM
* ajouter une interaction de type tap sur une forme OSM
* ajouter une interaction de type double tap sur une autre forme OSM
* exporter le dessin
* tester le dessin dans le lecteur
