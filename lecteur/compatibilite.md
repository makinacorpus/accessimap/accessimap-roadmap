# Compatibilité du lecteur

Le lecteur a été développé avec pour objectif de fonctionner sur le maximum de devices.

Basé sur des technos web (React), nous avons décliné le lecteur sous :

* web, utilisable plutôt avec Chrome sous Linux/Windows, et Firefox/Chrome sous Windows (voix plus agréables)
* desktop avec un empaquetage Electron qui permet de cible Windows et Linux
* Android avec un empaquetage Cordova

Attention, concernant les devices Android, vous devrez avoir une version >= 6.0.