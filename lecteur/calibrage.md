# Calibrage de l'écran

Chaque écran est différent, et nous devons paramétrer le lecteur
pour qu'il dispose de l'information sur la résolution et la taille physique de l'écran.

Pour cela, un menu calibrage est accessible.

En choisissant l'option de calibrage manuel,
un format de dessin (A3, A4, A5, A6, 19x13),
en positionnant le papier au bon format sur l'écran,
et en cliquant à la frontière du dessin à droite,
vous indiquerez au lecteur tout ce dont il a besoin pour comprendre
le ratio d'affichage à utiliser.

Alternativement, il existe une liste d'écrans pré-configurés par défaut, facilitant la
sélection d'un calibrage connu.
<br>
Cette liste est embarquées dans le code source et ne peut être modfiée.
Cependant, la version Windows de l'applciation permet d'en ajouter en plaçant
un fichier nommé `screen-presets.json` à coté de l'exécutable Windows.
<br>
Ce fichier doit contenir une liste de précalibrages sous forme JSON, comme illustré dans l'exemple qui suit :
```json
[
    {"name": "Précalibrage 100 dpi", "value": 100},
    {"name": "Précalibrage 200 dpi", "value": 200}
]
```

Chaque entrée possède 2 propriétés :
- `name` : Nom du précalibrage qui sera listé dans le menu de configuration
- `value` : La valeur correspondante à ce calibrage, en DPI.
  <br>
  Cette valeur (DPI) doit provenir du calibrage effectué avec l'application elle-même,
  car elle ne correspond pas toujours au PPI officiel de l'écran utilisé, en fonction
  du zoom général de Windows, du zoom appliqué par Chromium, ...

## Problèmes connus

Lors du calibrage sur des devices Android qui ont une police
de taille > 100% (Settings > Display > Font size),
le calibrage est relativement proportionnel à cette taille de police.

Pensez à vérifier que la font size est bien à 100%.

De la même manière sous Windows, le calibrage dépend de la valeur de mise à l'échelle utilisée
par le système d'exploitation.
