![Logo accessimap](./assets/logo_accessimap.png)

[Accéder au roadmap](http://accessimap-roadmap.surge.sh)

Le projet Accessimap est un projet de recherche
visant à améliorer l’accès aux cartographies pour les déficients visuels,
grâce à la conception d’interactions non-visuelles adaptées
permettant d’explorer des données libres (OpenStreetMap par exemple).

Ce gitbook vise à préciser ce qu'est le projet, de quoi il se compose,
et dans quelles directions nous allons, en terme de développement principalement.

