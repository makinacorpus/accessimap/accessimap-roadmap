# Sprints Janvier 2018

Concernant l'éditeur, nous avons poursuivi le travail au niveau de l'éditeur
concernant l'import de DERi.

Nous considérons que la configuration des interactions est satisfaisante
à notre niveau, sauf bug inconnu à ce jour.

Concernant le lecteur, nous avons produit la version Android.

Nous n'avons pas eu d'autres retour sur la version Electron,
et en l'état de nos tests, nous considérons le lecteur stable.

Nous pouvons poursuivre sur une facilitation de l'import des dessins,
mais l'utilisation d'une bibliothèque partagée nous paraîtra plus aisée.

## Editeur

* Remise en place des déploiements automatisés pour l'éditeur
* [Correction anomalie chargement OSM](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/315)
* [Mise en évidence de l'élément courant dans le panneau interactions](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/302)
* [Poursuite gestion undo/redo](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/287)
* [Améliorations import DERi - non finalisé](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/issues/282)

## Lecteur

* [Compilation du lecteur sous Android, accessible via les artifacts GitLab](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/99)
