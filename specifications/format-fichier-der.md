# Format des fichiers DER

Cette section décrit le format technique des fichiers sauvegardés par
l'éditeur de DER (**D**essin **E**n **R**elief), et interprétés par
le lecteur de DER.

## Conteneur

Un DER est une archive au format ZIP qui contient toutes les
ressources nécessaires à l'affichage du dessin en relief par
le lecteur, ou son édition par l'éditeur.

## Liste des fichiers

Les fichiers suivants sont toujours présents dans un DER :

- `Braille_2007.ttf` : Police de caractères pour l'affichage des
  texte en braille.
- `carte.png` : Fond de carte seul au format PNG.
- `carte_avec_source.svg` : DER au format vectoriel SVG, avec cadre de page et fond de carte.
- `carte_sans_source.svg` : DER au format vectoriel SVG, avec cadre de page *mais sans fond de carte*.
- `commentaires.txt` : Contient la description qui a été saisie dans `Paramètres` > `Commentaire de transcription`.
- `der.pdf` : DER au format PDF, avec cadre de page et fond de carte.
- `der.png` : DER au format PNG, avec cadre de page et fond de carte.
- `interactions.xml` : Paramétrage complet des interactions associées à ce DER.
- `legende.svg` : Légende au format vectoriel SVG, telle que définie dans `Légende`.

En complément, tout un ensemble de fichiers audios peuvent être présents si le DER contient des intéractions audios.
<br>
Par exemple :

- `MP3_sample_file_1.mp3`
- `MP3_sample_file_2.mp3`

## Structure des fichiers `carte_*_source.svg`

Ces 2 fichiers (`carte_avec_source.svg` et
`carte_sans_source.svg`) contiennent le coeur du dessin.
<br>
**Notes** :
* L'unique différence entre les deux est la présence
  d'une balise `<image>` supplémentaire contenant le fond de
  carte dans `carte_avec_source.svg` (cf le commentaire
  ajouté dans l'exemple ci-dessous pour illustration).
* Si vous incluez du texte dans le SVG, ayez conscience du fait
  que la police de caractère utilisée ne sera pas nécessairement
  présente sur l'appareil final qui lira le dessin. Quand cela
  se produit, l'affichage final est plus ou moins différent
  du dessin d'origine puisqu'une police de substition est
  utilisée à la place de celle que vous aviez choisie
  initialement.
  <br>
  Cela est particulièrement problématique pour le texte en
  braille que nous vous conseillons de rédiger en n'utilisant
  qu'exclusivement la police "Braille_2007" qui est fournie
  dans l'archive.

Exemple partiel de fichier :
```xml
<svg xmlns="http://www.w3.org/2000/svg" data-version="0.1" width="1122" height="793" viewBox="191 44 1122 793" style="overflow: visible;">
    <svg data-name="defs">
        <defs> [...] </defs>
    </svg>
    <metadata data-name="data-geojson" data-value=" [...] "/>
    <metadata data-name="data-model" data-value=" [...] "/>

    <!-- La balise <image> suivante n'est présente que dans "carte_avec_source.svg" : -->
    <image width="1122" height="793" x="191" y="44" [...] />

    <svg data-name="background" style="overflow: visible;"> [...] </svg>
    <svg data-name="geojson" style="overflow: visible;"> [...] </svg>
    <svg data-name="drawing" style="overflow: visible;"> [...] </svg>
    <svg data-name="overlay" style="overflow: visible;"> [...] </svg>
</svg>
```

Les différentes parties du fichier son détaillées dans les sections ci-dessous.

### Section définitions : `<svg data-name="defs">`

Cette balise possède un enfant `defs` qui contient toutes les
définitions de marqueurs et motifs visuels
(hachurages, textures, etc.) utilisables par les formes du dessin.

Cette balise suit les spécification SVG. En quelques mots :
Marqueurs et motifs doivent avoir un idenfiant unique `id` pour
pouvoir être référencés par la suite.

Chaque marqueur est déclaré grâce à une balise `marker`. Exemple :
```xml
<marker id="arrowStartMarker" refX="5" refY="5" markerWidth="10" markerHeight="10" orient="auto">
    <path d="M9,1 L5,5 9,9" style="fill:none;stroke:#000000;stroke-opacity:1"/>
</marker>
```

Chaque motif est déclaré via la balise `pattern`. Exemple :
```xml
<pattern id="bighash" patternUnits="userSpaceOnUse" width="20" height="20">
    <path d="M 10, 0 l 0, 20" stroke-width="2" shape-rendering="auto" stroke="#343434" stroke-linecap="square"/>
</pattern>
```

### Section meta-données GeoJSON : `<metadata data-name="data-geojson">`

Cette balise embarque les données GeoJSON contenant les
données importées depuis Open Street Map.

Tout le GeoJSON est encodé au format JSON dans la propriété
`data-value`. Exemple partiel :

```xml
<metadata data-name="data-geojson" data-value="[{&quot;id&quot;:&quot;places&quot;,&quot;name&quot;:&quot;Places&quot;,&quot;type&quot;:&quot;polygon&quot;,&quot;layer&quot;:{&quot;type&quot;:&quot;FeatureCollection [...] color&quot;:&quot;red&quot;,&quot;$$hashKey&quot;:&quot;object:33&quot;},&quot;$$hashKey&quot;:&quot;object:253&quot;}]"/>
```

### Section meta-données générales : `<metadata data-name="data-model">`

Cette balise contient toutes les informations et configurations
générales du dessin.

Toutes ces informations sont encodées au format JSON dans la propriété
`data-value`. Exemple partiel :
```xml
<metadata data-name="data-model" data-value="{&quot;title&quot;:&quot;Titre du dessin&quot;,&quot;isMapVisible&quot;:true, [...] ,&quot;mapIdVisible&quot;:&quot;osm&quot;}"/>
```

Voici un exemple décodé et lisible de la structure complète de
ce que peut contenir `data-value` :
```json
{
  "title": "Titre du dessin",
  "isMapVisible": true,
  "comment": "Commentaires transcription",
  "mapFormat": "landscapeA4",
  "legendFormat": "portraitA5",
  "backgroundColor": {
    "name": "Transparent",
    "color": "none"
  },
  "backgroundStyle": {
    "id": "solid",
    "name": "Fond uni",
    "style": [
      {
        "k": "fill-pattern",
        "v": "solid"
      }
    ]
  },
  "center": {
    "lat": 43.6042,
    "lng": 1.4410
  },
  "zoom": 13,
  "mapIdVisible": "osm"
}
```

### Section fond de carte : `<image>`

Cette balise n'est présente que dans le
fichier `carte_avec_source.svg`.

Elle contient le fond de la carte, sous forme d'une image encodée en
base64.

Exemple partiel :
```xml
<image width="1122" height="793" x="191" y="44"
    xmlns:xlink="http://www.w3.org/1999/xlink"
    xlink:href="data:image/png;base64,iVBORw0KGgoAAAAN [...] AAAAAElFTkSuQmCC"/>
```

### Section calque d'arrière-plan : `<svg data-name="background">`

Cette balise contient le fond uni d'arrière-plan du dessin.

Exemple :
```xml
<svg data-name="background" style="overflow: visible;">
    <g data-name="background-layer" id="background-layer" transform="translate(-173,0) scale(1,1) ">
        <g transform="translate(364,44)">
            <path d="M 0 0 L 1122 0 L 1122 793 L 0 793 L 0 0 z" id="background-path" class="notDeletable" style="pointer-events: none; fill: none;"/>
        </g>
    </g>
</svg>
```

### Section géométries GeoJSON : `<svg data-name="geojson">`

Cette balise contient les calques des formes vectorielles
qui correspondent aux données GeoJSON stockées dans la section
`data-geojson` décrite plus haut.

Sa structure minimale est la suivante :
```xml
<svg data-name="geojson" style="overflow: visible;">
    <g data-name="geojson-layer" id="geojson-layer">
        <g data-name="polygons-layer"/>
        <g data-name="lines-layer"/>
        <g data-name="points-layer"/>
        <g data-name="texts-layer"/>
    </g>
</svg>
```

Chacun des calques (`polygons-layer`, `lines-layer`,
`points-layer` et `texts-layer`) peut contenir des formes
vectorielles standards. Par exemple :
```xml
<svg data-name="geojson" style="overflow: visible;">
    <g data-name="geojson-layer" id="geojson-layer" transform="translate(107,0) scale(1,1) ">
        <g data-name="polygons-layer">
            <g class="vector rotable" id="places" fill="url(#bighash_red)" stroke="black" stroke-width="2">
                <path data-type="polygon" data-from="osm" class="places link_22689429" data-link="22689429" name="Place de l'Europe" e-style="bighash" e-color="red" d="M864,357L869,355L872,353L873,353L873,354L874,356L875,358L874,359L867,362L867,362L866,362L864,357Z" style="cursor: crosshair;">
                    <title>Place de l'Europe</title>
                </path>
            </g>
        </g>
        <g data-name="lines-layer"> [...] </g>
        <g data-name="points-layer">
            <g class="vector rotable" id="node_5696935220" stroke="black" stroke-width="2" fill="white">
                <path data-type="point" data-from="osm" class="node_5696935220" name="Le Shaker" cx="666" cy="330" d="M 666 330 m -10, 0 a 10,10 0 1,0 20,0 a 10,10 0 1,0 -20,0" style="cursor: crosshair;" data-link="618754778"/>
            </g>
        </g>
        <g data-name="texts-layer"> [...] </g>
    </g>
</svg>
```

**Important** : Les objets vectoriels auxquels des interactions sont
associées doivent impérativement avoir une propriété `data-link`
constituant leur identifiant unique numérique. C'est cet idenfiant
qui fait le lien entre un objet vectoriel et ses interactions.

### Section dessin : `<svg data-name="drawing">

Cette section fonctionne de la même façon que la précédente
(`geojson`), avec un calque supplémentaire pour les images.

Sa structure minimale est la suivante :
```xml
<svg data-name="drawing" style="overflow: visible;">
    <g data-name="drawing-layer" id="drawing-layer">
        <g data-name="images-layer"/>
        <g data-name="polygons-layer"/>
        <g data-name="lines-layer"/>
        <g data-name="points-layer"/>
        <g data-name="texts-layer"/>
    </g>
</svg>
```

Chacun des calques (`images-layer`, `polygons-layer`, `lines-layer`,
`points-layer` et `texts-layer`) peut contenir des formes
vectorielles standards.

Comme pour la section précédente (`geojson`), tous les objets
vectoriels auxquels des interactions sont associées doivent
impérativement avoir une propriété `data-link` unique.

**Note** : Le calque `images-layer` contient les images
importées dans le dessin. Elles embarquent les visuels au
format base64. Exemple partiel :
```xml
<g data-name="images-layer">
    <image x="679" y="169" width="750" height="562"
        xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="data:image/jpeg;base64,/9j/4AAQSkZJRgAB [...] 0ywCLCEBRHLXP//Z" data-link="5" class="link_5" style="cursor: crosshair;" transform="translate(-278,67)"/>
</g>
```

Tout commes les autres formes vectorielles sur lesquelles des
intéractions peuvent être ajoutées, les images de ce calque
doivent avoir une propriété `data-link` unique si des intéractions
lui sont associées.

### Section calque de premier-plan : `<svg data-name="overlay">`

Cette section contient les éléments visuels de premier plan.

Sa structure est la suivante (exemple partiel) :
```xml
<svg data-name="overlay" style="overflow: visible;">
    <g transform="translate(-173,0) scale(1,1) ">
        <g id="margin-layer" [...] > [...] </g>
        <g id="frame-layer" [...] > [...] </g>
        <g id="filter-menu-placeholder" [...] > [...] </g>
        <g id="main-menu-placeholder"  [...] > [...] </g>
    </g>
</svg>
```

Détail des calques :
- `margin-layer` : Contient la marge blanche constituant le
  pourtour de la page du DER, et l'ombrage de la zone en
  dehors de la page du DER.
- `frame-layer` : Contient le cadre noir délimitant le DER.
- `filter-menu-placeholder` : Contient le bouton d'accès aux
  filtres du DER, destiné au lecteur de DER.
  <br>
  Le rôle de cet élément est de pouvoir personnaliser la position
  du bouton "Filtres" (et son visuel) pour qu'il ne gène pas
  le DER.
  <br>
  **Note** : Cet élément est facultatif. S'il est omis, le lecteur
  affichera un bouton par défaut.
- `main-menu-placeholder` : Contient le bouton d'accès au menu
  des réglages du lecteur de DER.
  <br>
  Le rôle de cet élément est de pouvoir personnaliser la position
  du bouton "Menu" (et son visuel) pour qu'il ne gène pas
  le DER.
  <br>
  **Note** : Cet élément est également facultatif. S'il est omis,
  le lecteur affichera un bouton par défaut.

## Structure du fichier `interactions.xml`

Ce fichier contient la configuration de toutes les
intéractions d'un DER, ainsi que les filtres qui leurs sont
associés.

Voici un exemple de fichier minimal :
```xml
<?xml version="1.0" encoding="UTF-8"?>
<config xmlns="http://www.w3.org/1999/xhtml">
    <filters>
        <filter id="ce249aa9-95af-4ca0-8d7b-b162835ade64" name="Défaut"></filter>
    </filters>
    <pois>
        <poi id="poi-1" label="Mon POI" is-common-to-all-filters="false">
            <actions>
                <action uuid="33628ab5-fb34-44e8-99d4-cb8a4338f38e" gesture="tap" filter="ce249aa9-95af-4ca0-8d7b-b162835ade64" protocol="tts" value="TTS clic"></action>
            </actions>
        </poi>
    </pois>
</config>
```

L'élément racine `config` comprend 2 sections :
- `filters` : C'est ici que sont définis tous les filtres utilisés sur le dessin.
  <br>
  Chaque filtre possède 2 propriétés :
  * `id` : Identifiant unique du filtre.
  * `name` : Label du filtre, affiché sur le lecteur.
- `pois` : C'est ici que sont listés les objets interactifs, ou "POI".
  <br>
  Chaque POI (balise `poi`) possède les propriétés suivantes :
  <br>
  * `id` : Cet identifiant est une chaîne de caractères
    préfixée par "`poi-`", et suivie de l'identifiant unique
    de la forme vectorielle correspondante sur le dessin
    (c'est à dire la propriété `data-link` de la forme
    vectorielle).
  * `label` : Nom lisible du POI, utilisé pour faciliter
    l'édition des intéractions dans l'éditeur.
  * `is-common-to-all-filters` : Propriété à "true" ou "false"
    indiquant à l'éditeur si ce POI est consitutés de filtres
    communs à tous les filtres ou non.
    <br>
    Note : Cette propriété est nécessaire pour l'éditeur
    seulement et n'a pas d'impact sur le lecteur.
    C'est l'éditeur qui s'assure que tous les filtres
    contiennent les mêmes interactions lorsque cette
    fonctionnalité est activée.

  Chaque POI possède un enfant `actions` regroupant la liste
  des intéractions liées à ce POI.
  <br>
  Chaque interaction (balise `action`) possède les propriétés
  suivantes :
  * `uuid` : Identifiant unique universel de l'interaction.
  * `gesture` : Indique le type d'interaction. Valeurs possibles :
    - `tap` : Appui simple.
    - `double_tap` : Double appui.
    - `long_tap` : Appui long.
  * `filter` : Identifiant unique du filtre s'appliquant à
    cette interaction.
     <br>
     Doit référencer l'un des filtres déclarés plus haut
     (`config > filters > filter[id]`).
  * `protocol` : Type d'action à exécuter. Valeurs possibles :
    - `tts` : Lire une phrase via synthèse vocale.
    - `mp3` : Lire un fichier audio.
  * `value` : Dépend de la propriété `protocol`.
    <br>
    Si `protocol` vaut `tts`, alors ceci contient le texte à lire
    par la synthèse vocale.
    <br>
    Si `protocol` vaut `mp3`, alors ceci contient le nom du fichier audio à jouer.
