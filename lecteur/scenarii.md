# Scénarii de tests

Ces scénarii doivent être réalisés sur les 3 plate-formes générées :

* web
* electron
* android

Et sur l'écran tactile 10 points.

## Chargement d'un DERi

A partir de la bibliothèque des DERi accessible
[ici](https://gitlab.com/makinacorpus/accessimap/accessimap-bibliotheque),
charger les différents DERi suivant et constater leur bon fonctionnement :

* [Londres.zip](https://gitlab.com/makinacorpus/accessimap/accessimap-bibliotheque/blob/master/Londres.zip)
* [Exemple.multi.filtres.zip](https://gitlab.com/makinacorpus/accessimap/accessimap-bibliotheque/blob/master/Exemple.multi.filtres.zip)
* [EssaiDoubleInteraction.zip](https://gitlab.com/makinacorpus/accessimap/accessimap-bibliotheque/blob/master/EssaiDoubleInteraction.zip)

## Utilisation d'un DERi

* le zoom / pinch ne doit pas provoquer de zoom du DERi
* lorsque je pose ma main sur la table, la détection du touch / double touch fonctionne
