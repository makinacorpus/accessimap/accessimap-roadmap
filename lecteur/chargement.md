# Chargement des DERi

## Electron / Web

Sur les versions Electron & Web, vous pouvez charger les DERi
à partir du menu 'Charger un document en relief'.

La boîte de dialogue d'exploration des fichiers vous permet d'accéder
aux DERi disponible sur votre espace disque.

## Android

La manoeuvre est différente sur Android.

L'explorateur de fichiers ne vous permet pas d'accéder facilement aux DERi
présents sur la tablette.

Un contournement est de disposer les DERi sur votre drive Google,
et d'y accéder ensuite à partir du menu de chargement de DERi.

# Fabrication des DERi

Ce paragraphe est à prendre en considération 
si le DERi n'est pas fabriqué à partir de l'éditeur, 
mais qu'il est construit manuellement par l'équipe de l'IJA ou autre.

Lors de l'enregistrement du fichier XML des interactions,
l'encodage du fichier doit être `UTF-8 without BOM`.

Le [Byte order mark](https://en.wikipedia.org/wiki/Byte_order_mark)
est un caractère qui peut être écrit lors de l'enregistrement du fichier
par votre éditeur de texte, ou par le programme qui générerait le fichier.

Cependant, comme vu dans l'issue [#108](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/issues/108),
ce caractère provoque une anomalie lors de l'ouverture du DERi.

Attention donc.