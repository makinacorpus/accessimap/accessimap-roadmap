# Editeur

Le code source est accessible sur [gitlab](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/).

L'état des tâches en cours est accessible [ici](https://gitlab.com/makinacorpus/accessimap/accessimap-editeur-der/boards/).

La version de validation Makina est accessible [ici](http://accessimap-editeur-der-staging.surge.sh/).

La version de validation IJA / IRIT est accessible [ici](http://accessimap-editeur-der.surge.sh/).

La version Makina est déployée au fil des modifications GitLab.

Afin de ne pas polluer l'utilisation IJA / IRIT,
Makina avertira lors de la mise à jour de de la version en ligne IJA / IRIT.

## Objectifs

L'objectif validé au COPIL du 20/11/2017 est de stabiliser l'éditeur sur :

* la définition d'interactions sur un Dessin En Relief
* l'import / export de DERi

Ces deux points nous permettront d'atteindre la V0 de l'éditeur
au cours du premier semestre 2018.

Une fois cette V0 validée, nous prendrons le temps pour mettre par écrit
les fonctionnalités de l'éditeur, ce qui représentera un cahier de spécifications.
(rétro conception)
