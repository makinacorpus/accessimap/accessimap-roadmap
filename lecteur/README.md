# Lecteur

Accessible sur https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/.

L'état des tâches en cours est accessible [ici](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/boards/).

Les versions du lecteur sont les suivantes :

|Version | Pour makina | Pour IJA / IRIT |
|--------|-------------|-----------------|
|Web     |[version master](http://accessimap-lecteur-der-testing.surge.sh/#/)|[version stabilisée](http://accessimap-lecteur-der-staging.surge.sh/#/)&nbsp;&nbsp;[zip web master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_der-reader)|
|Electron Windows |[version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_electron_windows)| [version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_electron_windows)|
|Electron Linux   |[version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_electron_linux)| [version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_electron_linux)| 
|Android |[version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_android)|[version master](https://gitlab.com/makinacorpus/accessimap/accessimap-lecteur-der/-/jobs/artifacts/master/download?job=build_android)|

Concernant la version *Web*, privilégier le navigateur Chrome
pour bénéficier de voix plus agréables.

Concernant la version *Electron Windows*, 
l'exécutable Windows (7, 10, 10 Pro) peut ne pas être considéré comme fiable.
Dans ce cas, aller sur les propriétés du fichier accessimap-lecteur-der.exe,
puis rendre l'exécution possible. Ce problème n'est pas systématique.

Les versions *Electron* et *Android* n'étant pas considérées comme stabilisées,
les versions téléchargeables par l'IJA / IRIT sont issues de la version master.

Afin de ne pas polluer l'utilisation IJA / IRIT,
Makina avertira lors de la mise à jour de de la version en ligne IJA / IRIT.


## Objectifs

# Au 20/11/2017

L'objectif validé au COPIL du 20/11/2017 est de stabiliser le lecteur sur :

* l'utilisation sur Windows + écran IIYAMA tactile 27"
* l'utilisation sur tablette Android

La stabilisation sur Windows doit être validée d'ici la fin de l'année,
alors que la partie Android sur le premier semestre 2018.

Comme pour l'éditeur, une fois la V0 atteinte, nous prendrons le temps pour
mettre par écrit les fonctionnalités du lecteur.

Nous aurons ainsi un cahier de spécifications commun au lecteur et à l'éditeur.

Une fois ce cahier de spécifications écrit, nous pourrons critiquer
les deux projets afin de définir les nouvelles étapes, à la fois en terme
de fonctionnalités et d'infrastructure technique et logicielle.

# Au 25/06/2018

Des anomalies subsistent encore au niveau du lecteur,
et de nouveaux sprints doivent nous permettre de les corriger.

Des améliorations d'utilisations sont prévues, 
avec pour objectif l'utilisation du lecteur Makina au festival de rue 
de Ramonville en Septembre.